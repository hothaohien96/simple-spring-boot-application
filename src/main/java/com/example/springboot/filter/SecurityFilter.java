package com.example.springboot.filter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Base64;


@Component
@Order(1)
public class SecurityFilter implements Filter {
    static final String SECURE_TOKEN_HEADER = "SECURE_TOKEN";
    static final String TOKEN_VALUE = "0987654321";
    Logger logger = LoggerFactory.getLogger(SecurityFilter.class);

    @Override
    public void doFilter(
            ServletRequest request,
            ServletResponse response,
            FilterChain chain) throws IOException, ServletException {
        logger.info("Security filter for demo project (ORDER 1)");

        String encodedString = ((HttpServletRequest) request).getHeader(SECURE_TOKEN_HEADER);

        if (encodedString == null) {
            HttpServletResponse response1 = (HttpServletResponse) response;
            response1.setStatus(HttpStatus.UNAUTHORIZED.value());
            PrintWriter writer = response1.getWriter();
            writer.println("Missing security token in the header.");
            return;
        }

        // Decode the token values
        byte[] decodedBytes = Base64.getDecoder().decode(encodedString);
        String decodedString = new String(decodedBytes);
        logger.info("Decoding security token: " + decodedString);

        // Validate the token value
        if (decodedString.equals(TOKEN_VALUE)){
            chain.doFilter(request, response);
        } else {
            HttpServletResponse response1 = (HttpServletResponse) response;
            response1.setStatus(HttpStatus.UNAUTHORIZED.value());
            PrintWriter writer = response1.getWriter();
            writer.println("Wrong security token value!");
        }
    }
}
