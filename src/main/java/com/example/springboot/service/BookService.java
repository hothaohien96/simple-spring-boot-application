package com.example.springboot.service;

import com.example.springboot.entity.Book;
import com.example.springboot.repository.BookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.DeleteMapping;

import org.springframework.web.client.HttpClientErrorException;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

 @Service
 public class BookService {

        @Autowired
        private BookRepository bookRepository;

        public ResponseEntity<Book> getBookById(Long bookId)
                throws HttpClientErrorException {
            Book book = bookRepository.findById(bookId)
                    .orElseThrow(() -> new HttpClientErrorException(HttpStatus.NOT_FOUND));
            return ResponseEntity.ok().body(book);
        }

    public List<Book> getAllBooks() {
        return bookRepository.findAll();
    }

    public Book createBook(Book book) {
        return bookRepository.save(book);
    }

    public ResponseEntity<Book> updateBook(Long bookId,
                                           Book bookDetails) throws HttpClientErrorException {
        Book book = bookRepository.findById(bookId)
                .orElseThrow(() -> new HttpClientErrorException(HttpStatus.NOT_FOUND));

        book.setTitle(bookDetails.getTitle());
        book.setDescription(bookDetails.getDescription());
        final Book updatedBook = bookRepository.save(book);
        return ResponseEntity.ok(updatedBook);
    }


    @DeleteMapping("/books/{id}")
    public Map<String, Boolean> deleteBook(Long bookId)
            throws HttpClientErrorException {
        Book book = bookRepository.findById(bookId)
                .orElseThrow(() -> new HttpClientErrorException(HttpStatus.NOT_FOUND));

        bookRepository.delete(book);
        Map<String, Boolean> response = new HashMap<>();
        response.put("deleted", Boolean.TRUE);
        return response;
    }
}
