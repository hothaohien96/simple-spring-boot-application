package com.example.springboot.controller;

import com.example.springboot.entity.Book;
import com.example.springboot.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.HttpClientErrorException;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/v1")
public class BookController {

    @Autowired
    private BookService bookService;

    @GetMapping("/books/{id}")
    public ResponseEntity<Book> getBookById(@PathVariable (value="id") Long bookId)
            throws HttpClientErrorException {
        return bookService.getBookById(bookId);
    }

    @GetMapping("/books")
    public List<Book> getAllBooks() {
        return bookService.getAllBooks();
    }

    @PostMapping("/books")
    public Book createBook(@RequestBody Book book) {
        return bookService.createBook(book);
    }

    @PutMapping("/books/{id}")
    public ResponseEntity<Book> updateBook(@PathVariable (value="id")  Long bookId,
                                           @RequestBody Book bookDetails) throws HttpClientErrorException {
        return bookService.updateBook(bookId, bookDetails);
    }

    @DeleteMapping("/books/{id}")
    public Map<String, Boolean> deleteBook(@PathVariable (value="id")  Long bookId)
            throws HttpClientErrorException {
      return bookService.deleteBook(bookId);
    }
}
