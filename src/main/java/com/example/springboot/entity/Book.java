package com.example.springboot.entity;

import com.sun.istack.NotNull;

import javax.persistence.*;
//Entities in JPA are nothing but Plain Old Java Object representing data that can be persisted to the database.
//An entity represents a table stored in a database. Every instance of an entity represents a row in the table
@Entity
public class Book {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(name = "title")
    private String title;

    @Column(name = "description")
    private String description;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
